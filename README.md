#bcompiler

参考：
 php5.5 中

zend_op_array 结构   Zend/zend_compile.h

struct _zend_op_array {
        
        /* Common elements */

        zend_uchar type;
        const char *function_name;
        zend_class_entry *scope;
        zend_uint fn_flags;
        union _zend_function *prototype;
        zend_uint num_args;
        zend_uint required_num_args;
        zend_arg_info *arg_info;
        
        /* END of common elements */

        zend_uint *refcount;

        zend_op *opcodes;
        zend_uint last;

        zend_compiled_variable *vars;
        int last_var;

        zend_uint T;

        zend_uint nested_calls;
        zend_uint used_stack;

        zend_brk_cont_element *brk_cont_array;
        int last_brk_cont;

        zend_try_catch_element *try_catch_array;
        int last_try_catch;
        zend_bool has_finally_block;

        /* static variables support */
        HashTable *static_variables;

        zend_uint this_var;

        const char *filename;
        zend_uint line_start;
        zend_uint line_end;
        const char *doc_comment;
        zend_uint doc_comment_len;
        zend_uint early_binding; /* the linked list of delayed declarations */

        zend_literal *literals;
        int last_literal;

        void **run_time_cache;
        int  last_cache_slot;

        void *reserved[ZEND_MAX_RESERVED_RESOURCES];
};




一、定义的宏不存在

在Zend/zend.h

/* data types */
/* All data types <= IS_BOOL have their constructor/destructors skipped */
#define IS_NULL         0
#define IS_LONG         1
#define IS_DOUBLE       2
#define IS_BOOL         3
#define IS_ARRAY        4
#define IS_OBJECT       5
#define IS_STRING       6
#define IS_RESOURCE     7
#define IS_CONSTANT     8
#define IS_CONSTANT_ARRAY       9
#define IS_CALLABLE     10


/*#ifdef ZEND_ENGINE_2_4
	case IS_CLASS:
	case IS_SCALAR:
	case IS_NUMERIC:
		break;
#endif*/

二、 error: ‘zend_arg_info’ has no member named ‘return_reference’

-  SERIALIZE_SCALAR(arg_info->return_reference , zend_bool);
+  SERIALIZE_SCALAR(arg_info->fn_flags & ZEND_ACC_RETURN_REFERENCE , zend_bool);




https://bugs.php.net/patch-display.php?bug_id=61187&patch=php-pecl-parsekit-1.3-php-5.4.patch&revision=latest

https://bugs.php.net/bug.php?id=61674

https://github.com/derickr/xdebug/blob/f4d27aa14d829db586b59ab8139b55b07d251d4e/xdebug.c
